describe('Test Describe',()=>{
    it('Should login with correct credentials',()=>{
        cy.login(Cypress.env('USERNAME'),Cypress.env('PASSWORD'))
        cy.fixture('messages').then(messageObject=>{
            cy.get('[id="content"] h4')
            .should('exist')
            .and('be.visible')
            .invoke('text')
            .then(text=> {
               expect(text).to.eq(messageObject.successMessageWhenLogin)
            })
        })        
    })

    it('Should login with uncorrect credentials',()=>{
        cy.login('tomsmith','gdgdgd!')
        cy.get('[id="flash"]')
         .should('exist')
         .and('be.visible')
         .invoke('text')
         .then(text=> {
            cy.wrap(text).should('contains','Your password is invalid')
            expect(text).to.contains('Your password is invalid')
         })
    })    
})