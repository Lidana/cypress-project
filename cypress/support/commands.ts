/* JS Command 
    Cypress.Commands.add('login', (email, password) => { 
    cy.visit('/')
    cy.contains('a','Form Authentication').click()
    cy.get('#username').type(`${email}`)
    cy.get('#password').type(`${password}{enter}`) 
 })*/
 
 declare global {
  namespace Cypress {
    interface Chainable {
      /**
       * Custom command to login using the given username and passwords
       * @example cy.login('username','password')
       */
       login(email: string, password:string): void
    }
  }
}

export function login (email: string, password:string){
  cy.visit('/')
  cy.contains('a','Form Authentication').click()
  cy.get('#username').type(`${email}`)
  cy.get('#password').type(`${password}{enter}`)
}

Cypress.Commands.add('login',login)

export{}